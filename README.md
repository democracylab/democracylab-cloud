# admin.democracylab.io cloud

Based on: [template-docker-backend](https://gitlab.com/theplacelab/cloud/template-docker-backend)
Public: https:/admin.democracylab.io  
Portainer: https:/admin.democracylab.io:9000

### How to use

- Make changes here and then `./deploy` to push them to swarm
- "stacks" folder includes additional stacks which are manually added to Portainer, these should be added to the deploy script as well
- if you have anything with public ports, be sure to add that to the iptables.conf

### Setup

- Make sure you can get to your server and enable swarm

```shell
ssh root@admin.democracylab.io
docker swarm init --advertise-addr IP
docker network create -d overlay shared
```
## Setup iptables

- Install iptables
  `sudo apt-get install iptables-persistent`

 Move config in place:
```shell
 rsync -avzh ./iptables/iptables.conf root@165.227.16.247:/etc/iptables.conf
 rsync -avzh ./iptables/iptables.service root@165.227.16.247:/etc/systemd/system/
```
Load the rules:
```shell
 ssh root@admin.democracylab.io
 iptables-restore -n /etc/iptables.conf
```
Enable them on reboot:
```
 sudo systemctl enable iptables
 sudo systemctl start iptables
 reboot
```

### Setup SSL

- Generate primes:
````shell
sudo openssl dhparam -out /docker/stack-primary/dh-param/dhparam-2048.pem 2048```
````

- Issue certs:
  Assumes you are using [Namecheap](https://github.com/Neilpang/acme.sh/blob/master/dnsapi/dns_namecheap.sh), if you aren't, check the [docs here](https://github.com/Neilpang/acme.sh/tree/master/dnsapi). **Remember to whitelist the sever at namecheap.**

- Install acme.sh:
```shell
	apt-get install socat
	curl https://get.acme.sh | sh
```

- Aquire NAMECHEAP_API_KEY (after you have run this once, you can edit the values in `~/.acme.sh/account.conf)`
```shell
   export NAMECHEAP_API_KEY=''
   export NAMECHEAP_USERNAME=''
   export NAMECHEAP_SOURCEIP=''
   acme.sh --issue --dns dns_namecheap -d DOMAIN -d *.DOMAIN
```

### VPN
Recommend you secure SSH access via bastion VPN, which means you need to configure the VPN server to route the traffic for the machine you want to secure. You do this on OpenVPN, [here's how](https://andrewsempere.org/blog/2018/12/22/Securing-Docker-with-OpenVPN.html).
There's example scripts in 1password.
